﻿using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthYou.Classes.Logic
{
	public class Program : IProgram
	{
		IRepository _repo = Factory.Instance.GetRepository();

		public void AddProduct(Product product, int grams)
		{
			CheckDate();
			if (_repo.User.CurrentCondition.EatenProducts == null)
				_repo.User.CurrentCondition.EatenProducts = new List<EatenProduct>();
			var eatenProduct = new EatenProduct
			{
				KcalPerGram = product.KcalPerGram,
				Name = product.Name,
				KcalsEaten = product.KcalPerGram * grams
			};
			_repo.User.CurrentCondition.EatenProducts.Add(eatenProduct);
			CountCalories();
		}

		public void RemoveProduct(EatenProduct product)
		{

			_repo.User.CurrentCondition.EatenProducts.Remove(product);
			_repo.User.CurrentCondition.KcalsEaten -= product.KcalsEaten;
			CountCalories();
		}

		public void AddActivity(Activity activity, int time)
		{
			CheckDate();
			if (_repo.User.CurrentCondition.DoneExercises == null)
				_repo.User.CurrentCondition.DoneExercises = new List<DoneExercise>();
			var doneExercise = new DoneExercise
			{
				Name = activity.Name,
				BurningKcalsPerMinute = activity.BurningKcalsPerMinute,
				LeadTime = time,
				KcalsBurnt = activity.BurningKcalsPerMinute * time
			};
			_repo.User.CurrentCondition.DoneExercises.Add(doneExercise);
			//_repo.User.CurrentCondition.ActivityLevel += doneExercise.KcalsBurnt;
			CountActivityLevel();
		}

		public void RemoveActivity(DoneExercise activity)
		{
			_repo.User.CurrentCondition.DoneExercises.Remove(activity);
			_repo.User.CurrentCondition.ActivityLevel -= activity.KcalsBurnt;
			CountActivityLevel();
		}

		public void CountSleepingHours(int startHour, int startMinutes, int finishHour, int finishMinutes)
		{
			_repo.User.SleepingHours = 0;
			int start = startHour * 60 + startMinutes;
			int finish = finishHour * 60 + finishMinutes;
			if (start < finish)
				_repo.User.SleepingHours = finish - start;
			if (start > finish)
				_repo.User.SleepingHours = 1440 - start + finish;
		}

		public void CountGoals(string name)
		{
			if (name == "Lose weight")
			{
				_repo.User.Goals.Name = "Lose weight";
				_repo.User.Goals.ActivityLevel = 550;
				if (_repo.User.Gender == "male")
					_repo.User.Goals.KcalsEaten = 1.2 * (88.36 + (13.4 * _repo.User.Weight) + (4.8 * _repo.User.Height) - (5.7 * _repo.User.Age));
				if (_repo.User.Gender == "female")
					_repo.User.Goals.KcalsEaten = 1.2 * (447.6 + (9.2 * _repo.User.Weight) + (3.1 * _repo.User.Height) - (4.3 * _repo.User.Age));
			}

			if (name == "Gain weight")
			{
				_repo.User.Goals.Name = "Gain weight";
				_repo.User.Goals.ActivityLevel = 250;
				if (_repo.User.Gender == "male")
					_repo.User.Goals.KcalsEaten = 1.9 * (88.36 + (13.4 * _repo.User.Weight) + (4.8 * _repo.User.Height) - (5.7 * _repo.User.Age));
				if (_repo.User.Gender == "female")
					_repo.User.Goals.KcalsEaten = 1.9 * (447.6 + (9.2 * _repo.User.Weight) + (3.1 * _repo.User.Height) - (4.3 * _repo.User.Age));
			}
			if (name == "Become helthier")
			{
				_repo.User.Goals.Name = "Become helthier";
				_repo.User.Goals.ActivityLevel = 250;
				if (_repo.User.Gender == "male")
					_repo.User.Goals.KcalsEaten = 1.55 * (88.36 + (13.4 * _repo.User.Weight) + (4.8 * _repo.User.Height) - (5.7 * _repo.User.Age));
				if (_repo.User.Gender == "female")
					_repo.User.Goals.KcalsEaten = 1.55 * (447.6 + (9.2 * _repo.User.Weight) + (3.1 * _repo.User.Height) - (4.3 * _repo.User.Age));
			}
		}

		public void CheckDate()
		{
			if (_repo.PreviousTime.Year != DateTime.Now.Year)
				_repo.PreviousTime = DateTime.Now;
			if (_repo.PreviousTime != null)
			{
				if (_repo.PreviousTime.Day != DateTime.Now.Day)
				{
					_repo.User.CurrentCondition = new CurrentCondition();
					_repo.PreviousTime = DateTime.Now;
				}
			}
			else _repo.PreviousTime = DateTime.Now;
		}

		public Product SearchProduct(string name)
		{
			return _repo.Products.FirstOrDefault(p => p.Name == name);
		}

		public Activity SearchActivity(string name)
		{
			return _repo.Activities.FirstOrDefault(a => a.Name == name);
		}

		public void CountCalories()
		{
			_repo.User.CurrentCondition.KcalsEaten = 0;
			foreach (var product in _repo.User.CurrentCondition.EatenProducts)
			{
				_repo.User.CurrentCondition.KcalsEaten += product.KcalsEaten;
			}
		}

		public void CountActivityLevel()
		{
			_repo.User.CurrentCondition.ActivityLevel = 0;
			foreach (var activity in _repo.User.CurrentCondition.DoneExercises)
			{
				_repo.User.CurrentCondition.ActivityLevel += activity.KcalsBurnt;
			}
		}

		public double ActivityProgress()
		{
			try
			{
				if (_repo.User.Goals.ActivityLevel != 0)
				{
					double progress = Math.Round((_repo.User.CurrentCondition.ActivityLevel / _repo.User.Goals.ActivityLevel) * 100, 1);
					if (progress < 100)
						return progress;
					else return 100;
				}
				else return 0;
			}
			catch
			{
				return 0;
			}
		}

		public double CaloriesProgress()
		{
			try
			{
				if (_repo.User.Goals.KcalsEaten != 0)
				{
					double progress = Math.Round((_repo.User.CurrentCondition.KcalsEaten / _repo.User.Goals.KcalsEaten) * 100, 1);
					if (progress < 100)
						return progress;
					else return 100;
				}
				else return 0;
			}
			catch
			{
				return 0;
			}
		}

		public double SleepingProgress()
		{
			if (_repo.User.SleepingHours < 480)
				return Math.Round((_repo.User.SleepingHours / 480) * 100, 1);
			else return 100;
		}


	}
}
