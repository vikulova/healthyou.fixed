﻿using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthYou.Classes.Logic
{
	public class Repository : IRepository
	{
		Context _context = new Context();
		public User User { get; set; }
		public List<Product> Products { get; set; }
		public List<Activity> Activities { get; set; }
		public DateTime PreviousTime { get; set; }

		public Repository()
		{

		}

		public User Autorization(string email, string password)
		{
			User = _context.Users.FirstOrDefault(u => u.Email == email && u.Password == password);
			return User;
		}

		public bool RegisteredUser(string email)
		{
			bool registered = false;
			if (_context.Users.FirstOrDefault(u => u.Email == email) != null)
				registered = true;
			return registered;
		}

		public void Restore()
		{
			User = _context.Users.Include("Goals").Include("CurrentCondition").FirstOrDefault(u => u.Email == User.Email);
			Products = _context.Products.ToList();
			Activities = _context.Activities.ToList();
		}

		public void RestoreProducts()
		{
			Products = _context.Products.ToList();
		}

		public void RestoreActivities()
		{
			Activities = _context.Activities.ToList();
		}

		public void Save()
		{
			_context.SaveChanges();
		}

		public void SaveUser(User user)
		{
			_context.Users.Add(user);
			_context.SaveChanges();
		}

		public void AddNewProduct(Product product)
		{
			_context.Products.Add(product);
		}

		public void AddNewActivity(Activity activity)
		{
			_context.Activities.Add(activity);
		}
	}
}
