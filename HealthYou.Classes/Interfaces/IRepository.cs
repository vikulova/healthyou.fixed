﻿using HealthYou.Classes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthYou.Classes.Interfaces
{
	public interface IRepository
	{
		User User { get; set; }
		List<Product> Products { get; set; }
		List<Activity> Activities { get; set; }
		DateTime PreviousTime { get; set; }

		User Autorization(string email, string password);
		bool RegisteredUser(string email);
		void Restore();
		void RestoreProducts();
		void RestoreActivities();
		void Save();
		void SaveUser(User user);
		void AddNewProduct(Product product);
		void AddNewActivity(Activity activity);
	}
}
