﻿using HealthYou.Classes;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using HealthYou.Classes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для AddingActivitiesWindow.xaml
    /// </summary>
    public partial class AddingActivitiesWindow : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();
		IProgram _prog = new Program();

		public AddingActivitiesWindow()
        {
            InitializeComponent();
        }

        private void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            _prog.SearchActivity(SearchTextBox.Text);
			if (_prog.SearchActivity(SearchTextBox.Text) != null)
				SearchingActivitiesDataGrid.Items.Add(_prog.SearchActivity(SearchTextBox.Text));
			else
				MessageBox.Show("There is no such product in our base, however you can add it yourself");
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (SearchingActivitiesDataGrid.SelectedItem == null)
                MessageBox.Show("Please, select the product to add");

            if (SearchingActivitiesDataGrid.SelectedItem != null)
            {
                var activityInfoWindow = new ActivityInfoWindow(SearchingActivitiesDataGrid.SelectedItem as Activity);
                activityInfoWindow.Show();
                this.Close();
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            var amountOfActivitiesDoneWindow = new AmountOfActivitiesDoneWindow();
            amountOfActivitiesDoneWindow.Show();
            this.Close();
        }

        private void ButtonAddNew_Click(object sender, RoutedEventArgs e)
        {
            var newActivityWindow = new NewActivityWindow();
            newActivityWindow.Show();
            this.Close();
        }
    }
}
