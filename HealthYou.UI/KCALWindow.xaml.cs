﻿using HealthYou.Classes;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using HealthYou.Classes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для KCALWindow.xaml
    /// </summary>
    public partial class KCALWindow : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();
		IProgram _prog = new Program();

		public KCALWindow()
		{
			InitializeComponent();
			_repo.Restore();
			try
			{
				AlreadyEatenTextBlock.Text = _repo.User.CurrentCondition.KcalsEaten.ToString();
			}
			catch
			{
				AlreadyEatenTextBlock.Text = "0";
			}

			RecomendedCaloriesTextBlock.Text = _repo.User.Goals.KcalsEaten.ToString();
			PercentTextBlock.Text = _prog.CaloriesProgress().ToString() + " %";
			try
			{
				if (_repo.User.Goals.KcalsEaten > _repo.User.CurrentCondition.KcalsEaten)
					textBlockNeedToEat.Text = "You need to eat " +(_repo.User.Goals.KcalsEaten - _repo.User.CurrentCondition.KcalsEaten).ToString() + " calories";
				else textBlockNeedToEat.Text = "Daily rate fulfilled!";
			}
			catch
			{
				textBlockNeedToEat.Text = "You need to eat " + _repo.User.Goals.KcalsEaten.ToString() + " calories";
			}
		}

		private void ButtonBack_Click(object sender, RoutedEventArgs e)
		{
			var todayWindow = new TodayWindow();
			todayWindow.Show();
			this.Close();
		}

		private void ButtonAdd_Click(object sender, RoutedEventArgs e)
		{
			var addingCaloriesWindow = new AddingCaloriesWindow();
			addingCaloriesWindow.Show();
			this.Close();
		}

		private void ButtonEdit_Click(object sender, RoutedEventArgs e)
		{
			var amountOfEatenCaloriesToday = new AmountOfEatenCaloriesToday();
			amountOfEatenCaloriesToday.Show();
			this.Close();
		}
	}
}
