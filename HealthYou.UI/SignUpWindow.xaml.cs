﻿using HealthYou.Classes;
using HealthYou.Classes.Helpers;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using HealthYou.Classes.Models;
using HealthYou.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для SignUpWindow.xaml
    /// </summary>
    public partial class SignUpWindow : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();

		public SignUpWindow()
        {
            InitializeComponent();
        }

        private void ButtonSignUp_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(FullNameTextBox.Text))
            {
                MessageBox.Show("Please, enter your name and surname");
                FullNameTextBox.Focus();
                return;
            }

            if (_repo.RegisteredUser(EmailTextBox.Text) == true) 
            {
                MessageBox.Show("This email is already registered!");
                EmailTextBox.Focus();
            }

            if (ValidEmailAddress(EmailTextBox.Text) == false)
            {
                EmailTextBox.Focus();
                return;
            }

            if (string.IsNullOrWhiteSpace(PasswordBox.Password))
            {
                MessageBox.Show("Please, enter password");
                PasswordBox.Focus();
                return;
            }

            if (PasswordBox.Password.Length < 5)
            {
                MessageBox.Show("Password must be at least 5 characters long");
                PasswordBox.Focus();
                return;
            }

			var user = new User
			{
				FullName = FullNameTextBox.Text,
				Email = EmailTextBox.Text,
				Password = PasswordHelper.GetHash(PasswordBox.Password),
				Goals = new Classes.Models.Condition(),
				CurrentCondition = new CurrentCondition(),
				//Age=0,
				//Height=0,
				//SleepingHours=0,
				//Weight=0
            };
            _repo.SaveUser(user);
            
            var logInWindow = new MainWindow();
            logInWindow.Show();
            this.Close();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            var logInWindow = new MainWindow();
            logInWindow.Show();
            this.Close();
        }

        public bool ValidEmailAddress(string emailAddress)
        {
            if (emailAddress.Length == 0)
            {
                MessageBox.Show("Please, enter your email");
                return false;
            }

            if (emailAddress.IndexOf("@") > -1)
            {
                if (emailAddress.IndexOf(".", emailAddress.IndexOf("@")) > emailAddress.IndexOf("@"))
                    return true;
            }

            MessageBox.Show("Invalid input format of email! " +
                          "Example: 'someone@example.com' ");
            return false;
        }
    }
}
