﻿using HealthYou.Classes;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для ActivityWindow.xaml
    /// </summary>
    public partial class ActivityWindow : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();
		IProgram _prog = new Program();

		public ActivityWindow()
        {
            InitializeComponent();
			texBlockActivityPercant.Text = _prog.ActivityProgress().ToString()+" %";
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            var todayWindow = new TodayWindow();
            todayWindow.Show();
            this.Close();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            var addingActivitiesWindow = new AddingActivitiesWindow();
            addingActivitiesWindow.Show();
            this.Close();
        }

        private void ButtonEdit_Click(object sender, RoutedEventArgs e)
        {
            var amountOfActivitiesDoneWindow = new AmountOfActivitiesDoneWindow();
            amountOfActivitiesDoneWindow.Show();
            this.Close();
        }
    }
}
