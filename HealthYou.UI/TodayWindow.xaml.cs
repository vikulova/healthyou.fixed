﻿using HealthYou.Classes;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using HealthYou.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для TodayWindow.xaml
    /// </summary>
    public partial class TodayWindow : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();
		IProgram _prog = new Program();
		public TodayWindow()
		{
			InitializeComponent();
			_repo.Restore();
			WeightTextBlock.Text = _repo.User.Weight.ToString();
			SleepTextBlock.Text = Math.Round(_repo.User.SleepingHours/60,1).ToString();
			try
			{
				KCALTextBlock.Text = _repo.User.CurrentCondition.KcalsEaten.ToString();
			}
			catch
			{
				KCALTextBlock.Text = "0";
			}
			try
			{
				ActivitiesTextBlock.Text = _prog.ActivityProgress().ToString();
			}
			catch
			{
				ActivitiesTextBlock.Text = "0";
			}
		}

		private void ButtonProfile_Click(object sender, RoutedEventArgs e)
		{
			var profileWindow = new ProfileWindow();
			profileWindow.Show();
			this.Close();
		}

		private void ButtonKCAL_Click(object sender, RoutedEventArgs e)
		{
			var kcalWindow = new KCALWindow();
			kcalWindow.Show();
			this.Close();
		}

		private void ButtonSleep_Click(object sender, RoutedEventArgs e)
		{
			var sleepWindow = new SleepWindow();
			sleepWindow.Show();
			this.Close();
		}

		private void ButtonActivities_Click(object sender, RoutedEventArgs e)
		{
			var activityWindow = new ActivityWindow();
			activityWindow.Show();
			this.Close();
		}

		private void ButtonLogOut_Click(object sender, RoutedEventArgs e)
		{
			var logInWindow = new MainWindow();
			logInWindow.Show();
			this.Close();
		}
	}
}
